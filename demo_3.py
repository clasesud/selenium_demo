import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, TimeoutException

# Abrir el navegador
browser = webdriver.Chrome()
browser.maximize_window()

# Ir a la página web
browser.get('https://scholar.google.com/scholar?as_q=5g+nsa&as_epq=&as_oq=&as_eq=&as_occt=title&as_sauthors=&as_publication=&as_ylo=2018&as_yhi=2024&hl=en&as_sdt=0%2C5')

# Esperar a que los resultados se carguen completamente
WebDriverWait(browser, 10).until(
    EC.presence_of_element_located((By.XPATH, '//div[@class="gs_ri"]'))
)

# Número de páginas a iterar
num_pages = 3

# Lista para almacenar los datos extraídos
data = []

for _ in range(num_pages):
    # Encontrar todos los resultados de búsqueda en la página actual
    results = browser.find_elements(By.XPATH, '//div[@class="gs_ri"]')

    for result in results:
        try:
            # Extraer información como antes
            link_element = result.find_element(By.CSS_SELECTOR, '.gs_rt a')
            href = link_element.get_attribute('href')
            text = link_element.text
            authors_info = result.find_element(By.CLASS_NAME, 'gs_a').text
            try:
                cites_element = result.find_element(By.XPATH, './/a[contains(text(),"Cited by")]')
                cites = cites_element.text.split(' ')[2]
            except Exception:
                cites = '0'

            data.append({
                'href': href,
                'title': text,
                'authors': authors_info.split(' - ')[0],
                'cites': cites
            })
        except Exception as e:
            print(f"Error processing result: {e}")

    # Intentar hacer clic en el botón de la siguiente página
    try:
        next_page_button = browser.find_element(By.CSS_SELECTOR, '.gs_ico.gs_ico_nav_next')
        next_page_button.click()
        time.sleep(5)  # Esperar a que la página cargue
    except (NoSuchElementException, TimeoutException):
        print("No more pages or next page button not found.")
        break  # Salir del bucle si no se encuentra el botón de la siguiente página


# Mostrar los datos
for row in data:
    print(f"Title: {row['title']}, Href: {row['href']}, Authors: {row['authors']}, Cited by: {row['cites']}")

# Cerrar el navegador
browser.quit()
