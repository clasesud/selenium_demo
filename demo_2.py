import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Abrir el navegador
browser = webdriver.Chrome()
browser.maximize_window()

# Ir a la página web
browser.get('https://scholar.google.com/scholar?as_q=5g+nsa&as_epq=&as_oq=&as_eq=&as_occt=title&as_sauthors=&as_publication=&as_ylo=2018&as_yhi=2024&hl=en&as_sdt=0%2C5')

# Esperar a que los resultados se carguen completamente
WebDriverWait(browser, 10).until(
    EC.presence_of_element_located((By.XPATH, '//div[@class="gs_ri"]'))
)

# Encontrar todos los resultados de búsqueda
results = browser.find_elements(By.XPATH, '//div[@class="gs_ri"]')

# Lista para almacenar los datos extraídos
data = []

for result in results:
    try:
        # Encontrar el enlace y el texto dentro del elemento con clase 'gs_rt'
        link_element = result.find_element(By.CSS_SELECTOR, '.gs_rt a')
        href = link_element.get_attribute('href')
        text = link_element.text

        # Encontrar autores dentro del elemento con clase 'gs_a'
        authors_info = result.find_element(By.CLASS_NAME, 'gs_a').text

        # Intentar encontrar el número de citas
        try:
            cites_element = result.find_element(By.XPATH, './/a[contains(text(),"Cited by")]')
            cites = cites_element.text.split(' ')[2]  # Extraer solo el número de "Cited by X"
        except Exception:
            cites = '0'  # Asumir 0 citas si no se encuentra el elemento

        # Añadir la información a la lista
        data.append({
            'href': href,
            'title': text,
            'authors': authors_info.split(' - ')[0],  # La primera parte antes del guion contiene los autores
            'cites': cites
        })
    except Exception as e:
        print(f"Error processing result: {e}")

# Mostrar los datos
for row in data:
    print(f"Title: {row['title']}, Href: {row['href']}, Authors: {row['authors']}, Cited by: {row['cites']}")

# Cerrar el navegador
browser.quit()
