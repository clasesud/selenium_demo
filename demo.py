import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

# Abrir el navegador
browser = webdriver.Chrome()

# Maximizar la ventana del navegador
browser.maximize_window()

# Ir a una página web
browser.get('https://scholar.google.com/')

# Asegurarte de que la página ha cargado el campo de búsqueda
search_box = WebDriverWait(browser, 10).until(
    EC.presence_of_element_located((By.NAME, 'q'))
)

# Buscar el término deseado
search_box.send_keys('5G NSA')
search_box.submit()

# Dar tiempo para que los resultados se carguen
time.sleep(5)

# Encontrar todos los elementos de anclaje que están dentro de elementos con la clase "gs_rt"
anchor_elements = browser.find_elements(By.CSS_SELECTOR, '.gs_rt a')

# Iterar sobre cada elemento de anclaje y realizar acciones o imprimir sus atributos
for anchor in anchor_elements:
    href = anchor.get_attribute('href')  # Obtener el atributo href
    text = anchor.text  # Obtener el texto visible del enlace
    
    print(f'Href: {href}, Text: "{text}"')

# Esperar un poco antes de cerrar para ver los resultados
time.sleep(5)

# Cerrar el navegador
browser.quit()